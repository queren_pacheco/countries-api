<?php

use Illuminate\Support\Facades\Route;

Route::get('/countries', 'CountryController@index');
Route::get('/countries/count', 'CountryController@count');
