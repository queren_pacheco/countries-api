<?php
namespace App\Services;

use App\Repositories\CountryRepositoryInterface;


class CountryService
{
    private $repository;
    public function __construct(CountryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }
    public function getCountriesByName($filter)
    {
        $countries = null;

        if (isset($filter)) {
            $countries = $this->repository->findByName($filter);
        } else {
            $countries = $this->repository->findAll();
        }

        return $countries;
    }

    public function getCountriesCount(){
        return $this->repository->getCount();
    }
}
