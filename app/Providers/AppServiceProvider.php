<?php

namespace App\Providers;

use CountryService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\CountryRepositoryInterface',
            'App\Repositories\CountryRepository'
        );

        $this->app->bind(CountryService::class, function ($app) {
            return $app->make(CountryService::class);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
