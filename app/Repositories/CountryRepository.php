<?php

namespace App\Repositories;

use App\Models\Country;
use GuzzleHttp\Client;


class CountryRepository implements CountryRepositoryInterface
{

    private $client;
    public function __construct()
    {
        $this->client = new Client(['base_uri' => 'https://restcountries.eu']);
    }

    public function findAll()
    {
        $response = $this->client->request('GET', '/rest/v2/all?fields=name;capital;region;languages;currencies;flag');
        return $this->responseToModel($response);
    }

    public function findByName($name)
    {
        $countries = $this->findAll();
        $filteredCountries = [];

        foreach ($countries as $country) {
            if (str_contains(strtolower($country->name), strtolower($name))) {
                array_push($filteredCountries, $country);
            }
        }

        return $filteredCountries;
    }

    public function getCount(){
        return count($this->findAll());
    }

    private function responseToModel($response)
    {
        $jsonResponse = json_decode($response->getBody());
        $countries = [];
        foreach ($jsonResponse as $jsonCountry) array_push($countries, new Country($jsonCountry));
        return $countries;
    }
}
