<?php

namespace App\Repositories;

interface CountryRepositoryInterface
{
    public function findAll();
    public function findByName($name);
    public function getCount();
}
