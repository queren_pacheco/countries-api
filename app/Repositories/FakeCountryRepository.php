<?php

namespace App\Repositories;

use App\Models\Country;


class FakeCountryRepository implements CountryRepositoryInterface
{

    public function findAll()
    {
        $fakeData = '[{"currencies":[{"code":"EUR","name":"Euro","symbol":"€"}],"languages":[{"iso639_1":"es","iso639_2":"spa","name":"Spanish","nativeName":"Español"}],"name":"Spain","capital":"Madrid","region":"Europe"},{"currencies":[{"code":"AZN","name":"Azerbaijani manat"}],"languages":[{"iso639_1":"az","iso639_2":"aze","name":"Azerbaijani","nativeName":"azərbaycan dili"}],"name":"Azerbaijan","capital":"Baku","region":"Asia"},{"currencies":[{"code":"BYN","name":"New Belarusian ruble","symbol":"Br"},{"code":"BYR","name":"Old Belarusian ruble","symbol":"Br"}],"languages":[{"iso639_1":"be","iso639_2":"bel","name":"Belarusian","nativeName":"беларуская мова"},{"iso639_1":"ru","iso639_2":"rus","name":"Russian","nativeName":"Русский"}],"name":"Belarus","capital":"Minsk","region":"Europe"},{"currencies":[{"code":"KZT","name":"Kazakhstani tenge"}],"languages":[{"iso639_1":"kk","iso639_2":"kaz","name":"Kazakh","nativeName":"қазақ тілі"},{"iso639_1":"ru","iso639_2":"rus","name":"Russian","nativeName":"Русский"}],"name":"Kazakhstan","capital":"Astana","region":"Asia"},{"currencies":[{"code":"KGS","name":"Kyrgyzstani som","symbol":"с"}],"languages":[{"iso639_1":"ky","iso639_2":"kir","name":"Kyrgyz","nativeName":"Кыргызча"},{"iso639_1":"ru","iso639_2":"rus","name":"Russian","nativeName":"Русский"}],"name":"Kyrgyzstan","capital":"Bishkek","region":"Asia"},{"currencies":[{"code":"EUR","name":"Euro","symbol":"€"}],"languages":[{"iso639_1":"lt","iso639_2":"lit","name":"Lithuanian","nativeName":"lietuvių kalba"}],"name":"Lithuania","capital":"Vilnius","region":"Europe"},{"currencies":[{"code":"MOP","name":"Macanese pataca","symbol":"P"}],"languages":[{"iso639_1":"zh","iso639_2":"zho","name":"Chinese","nativeName":"中文 (Zhōngwén)"},{"iso639_1":"pt","iso639_2":"por","name":"Portuguese","nativeName":"Português"}],"name":"Macao","capital":"","region":"Asia"},{"currencies":[{"code":"PLN","name":"Polish złoty","symbol":"zł"}],"languages":[{"iso639_1":"pl","iso639_2":"pol","name":"Polish","nativeName":"język polski"}],"name":"Poland","capital":"Warsaw","region":"Europe"},{"currencies":[{"code":"UZS","name":"Uzbekistani so\'m"}],"languages":[{"iso639_1":"uz","iso639_2":"uzb","name":"Uzbek","nativeName":"Oʻzbek"},{"iso639_1":"ru","iso639_2":"rus","name":"Russian","nativeName":"Русский"}],"name":"Uzbekistan","capital":"Tashkent","region":"Asia"}]';
        return $this->responseToModel($fakeData);
    }

    public function findByName($name)
    {
        $countries = $this->findAll();
        $filteredCountries = [];

        foreach ($countries as $country) {
            if (str_contains(strtolower($country->name), strtolower($name))) {
                array_push($filteredCountries, $country);
            }
        }

        return $filteredCountries;
    }

    public function getCount(){
        return 100;
    }

    private function responseToModel($data)
    {
        $jsonResponse = json_decode($data);
        $countries = [];
        foreach ($jsonResponse as $jsonCountry) array_push($countries, new Country($jsonCountry));
        return $countries;
    }
}
