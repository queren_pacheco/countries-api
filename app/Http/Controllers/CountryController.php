<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\CountryService;

class CountryController
{
    private $countryService;
    public function __construct(CountryService $countryService)
    {
        $this->countryService = $countryService;
    }

    public function index(Request $request)
    {
        $filter = $request->input('q');
        return json_encode($this->countryService->getCountriesByName($filter));
    }

    public function count()
    {
        return $this->countryService->getCountriesCount();
    }
}
