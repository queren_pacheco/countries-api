<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use JsonSerializable;

class Country extends Model implements JsonSerializable
{
    public $name;
    public $capital;
    public $region;
    public $languages;
    public $currencies;
    public $flag;

    public function __construct($data)
    {
        foreach ($data as $key => $value) $this->{$key} = $value;
    }

    public function jsonSerialize()
    {
        return $this;
    }
}
