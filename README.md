
# Documentación

  

## ¿En qué consiste?

Es un proyecto Lavarel que actúa como una API REST muy sencilla. Actúa como intermediario entre el usuario y el API externa https://restcountries.eu.

## Estructura

El proyecto se estructura por capas en `controladores`, `servicios` y `repositorios` para una mayor escalabilidad y mantenibilidad.
Se ha definido la interfaz en el repositorio  **CountryRepositoryInterface** con dos implementaciones **CountryRepository** (trae los datos de la API externa) y **FakeCountryRepository** (tiene datos ya indicados).
Se puede cambiar a través de **AppServiceProvider** una implementación por otra.
	

	    $this->app->bind(
    		'App\Repositories\CountryRepositoryInterface',
    		'App\Repositories\CountryRepository'
    	);

## Endpoints
Devuelve el listado de países que contengan en su nombre el valor enviado por parámetro.

    /api/countries?q={countryName}

Devuelve la cantidad de paises en el mundo.

    /api/countries/count